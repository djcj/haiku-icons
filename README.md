Haiku Icons
===========

Exported to SVG format on Haiku with Icon-O-Matic.

Checkout source: `git clone git://git.haiku-os.org/haiku`<br>
Browse source: http://cgit.haiku-os.org/haiku/tree/data/artwork<br>
Commit: [bd4ea6563fe4e619c8a46fca637c7ef0984dbf65](http://cgit.haiku-os.org/haiku/commit/?id=bd4ea6563fe4e619c8a46fca637c7ef0984dbf65)

Github mirror: https://github.com/haiku/haiku
